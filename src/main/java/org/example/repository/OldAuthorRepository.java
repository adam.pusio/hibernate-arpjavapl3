package org.example.repository;

import lombok.AllArgsConstructor;
import org.example.model.Author;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Optional;

@AllArgsConstructor
//CRUD <- create read update delete
public class OldAuthorRepository {
    private SessionFactory sessionFactory;

    public void save(Author author){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(author);
        transaction.commit();
        session.close();
    }

    public Optional<Author> find(Integer id){
        Session session = sessionFactory.openSession();
        Author author = session.find(Author.class, id);
        session.close();
        return Optional.ofNullable(author);
    }

    public void update(Author author){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(author);
        transaction.commit();
        session.close();
    }

    public void delete(Integer id){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
//        find(id).ifPresent(author -> session.remove(author));
        find(id).ifPresent(session::remove);
        transaction.commit();
        session.close();
    }

//    public void find(...)
//    public void update(...)
}
